package eu.dnetlib.jobs.featureextraction.lda;

import eu.dnetlib.featureextraction.FeatureTransformer;
import eu.dnetlib.jobs.AbstractSparkJob;
import eu.dnetlib.support.ArgumentApplicationParser;
import org.apache.spark.SparkConf;
import org.apache.spark.ml.clustering.LDAModel;
import org.apache.spark.ml.clustering.LocalLDAModel;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;

public class SparkLDAInference extends AbstractSparkJob {

    private static final Logger log = LoggerFactory.getLogger(SparkLDAInference.class);

    public SparkLDAInference(ArgumentApplicationParser parser, SparkSession spark) {
        super(parser, spark);
    }

    public static void main(String[] args) throws Exception {

        ArgumentApplicationParser parser = new ArgumentApplicationParser(
                readResource("/jobs/parameters/ldaInference_parameters.json", SparkLDAInference.class)
        );

        parser.parseArgument(args);

        SparkConf conf = new SparkConf();

        new SparkLDAInference(
                parser,
                getSparkSession(conf)
        ).run();
    }

    @Override
    public void run() throws IOException {

        // read oozie parameters
        final String workingPath = parser.get("workingPath");
        final String outputPath = parser.get("outputPath");
        final String ldaModelPath = parser.get("ldaModelPath");
        final int numPartitions = Optional
                .ofNullable(parser.get("numPartitions"))
                .map(Integer::valueOf)
                .orElse(NUM_PARTITIONS);

        log.info("workingPath:     '{}'", workingPath);
        log.info("numPartitions:   '{}'", numPartitions);
        log.info("outputPath:      '{}'", outputPath);
        log.info("ldaModelPath:    '{}'", ldaModelPath);

        Dataset<Row> inputFeaturesDS = spark.read().load(workingPath + "/countVectorized");

        LDAModel ldaModel = LocalLDAModel.load(ldaModelPath);

        Dataset<Row> ldaTopicsDS = FeatureTransformer.ldaInference(inputFeaturesDS, ldaModel);

        ldaTopicsDS
                .write()
                .mode(SaveMode.Overwrite)
                .save(outputPath);

    }

}

