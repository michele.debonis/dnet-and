package eu.dnetlib.support;

import com.google.common.collect.Sets;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class ConnectedComponent implements Serializable {

    private HashSet<String> docs;
    private String ccId;
    private HashSet<Relation> simrels;

    public ConnectedComponent() {
    }

    public ConnectedComponent(String ccId, Set<String> docs, Set<Relation> simrels) {
        this.docs = new HashSet<>(docs);
        this.ccId = ccId;
        this.simrels = new HashSet<>(simrels);
    }

    public ConnectedComponent(Set<String> docs) {
        this.docs = new HashSet<>(docs);
        //initialization of id and relations missing
    }

    public ConnectedComponent(String ccId, Iterable<String> docs, Iterable<Relation> simrels) {
        this.ccId = ccId;
        this.docs = Sets.newHashSet(docs);
        this.simrels = Sets.newHashSet(simrels);
    }

    @Override
    public String toString() {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(this);
        } catch (IOException e) {
            throw new RuntimeException("Failed to create Json: ", e);
        }
    }

    public Set<String> getDocs() {
        return docs;
    }

    public void setDocs(HashSet<String> docs) {
        this.docs = docs;
    }

    public String getCcId() {
        return ccId;
    }

    public void setCcId(String ccId) {
        this.ccId = ccId;
    }

    public void setSimrels(HashSet<Relation> simrels) {
        this.simrels = simrels;
    }

    public HashSet<Relation> getSimrels() {
        return simrels;
    }
}
