package eu.dnetlib.support;

import java.io.Serializable;

public class CoAuthor implements Serializable {

    public String fullname;
    public String lastname;
    public String firstname;
    public String orcid;

    public CoAuthor(String fullname, String firstname, String lastname, String orcid) {
        this.fullname = fullname;
        this.lastname = lastname;
        this.firstname = firstname;
        this.orcid = orcid;
    }

    public CoAuthor() {
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getOrcid() {
        return orcid;
    }

    public void setOrcid(String orcid) {
        this.orcid = orcid;
    }

    @Override
    public boolean equals(Object o) {

        if (o == this)
            return true;

        if (!(o instanceof CoAuthor))
            return false;

        CoAuthor c = (CoAuthor) o;

        return this.fullname.concat(this.firstname).concat(this.lastname).concat(this.orcid)
                .equals(c.fullname.concat(c.firstname).concat(c.lastname).concat(c.orcid));

    }
}
