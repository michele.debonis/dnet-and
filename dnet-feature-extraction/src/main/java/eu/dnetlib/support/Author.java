package eu.dnetlib.support;

import org.apache.spark.ml.linalg.DenseVector;
import org.codehaus.jackson.annotate.JsonIgnore;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class Author implements Serializable {

    public String fullname;
    public String firstname;
    public String lastname;
    public List<CoAuthor> coAuthors;
    public String orcid;
    public String id;
    public Map<String, double[]> embeddings;

    public Map<String, double[]> getEmbeddings() {
        return embeddings;
    }

    public Author(String fullname, String firstname, String lastname, List<CoAuthor> coAuthors, String orcid, String id, Map<String, double[]> embeddings, String pubId) {
        this.fullname = fullname;
        this.firstname = firstname;
        this.lastname = lastname;
        this.coAuthors = coAuthors;
        this.orcid = orcid;
        this.id = id;
        this.embeddings = embeddings;
        this.pubId = pubId;
    }

    public void setEmbeddings(Map<String, double[]> embeddings) {
        this.embeddings = embeddings;
    }

    public String pubId;

    public Author() {
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public List<CoAuthor> getCoAuthors() {
        return coAuthors;
    }

    public void setCoAuthors(List<CoAuthor> coAuthors) {
        this.coAuthors = coAuthors;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPubId() {
        return pubId;
    }

    public void setPubId(String pubId) {
        this.pubId = pubId;
    }

    public String getOrcid() {
        return orcid;
    }

    public void setOrcid(String orcid) {
        this.orcid = orcid;
    }

    @JsonIgnore
    public boolean isAccurate() {
        return ((this.firstname != null) && (this.lastname != null) && !firstname.isEmpty() && !lastname.isEmpty());
    }
}
