//package eu.dnetlib.example
//
//import com.intel.analytics.bigdl.dllib.NNContext
//import com.intel.analytics.bigdl.dllib.keras.Model
//import com.intel.analytics.bigdl.dllib.keras.models.Models
//import com.intel.analytics.bigdl.dllib.keras.optimizers.Adam
//import com.intel.analytics.bigdl.dllib.nn.ClassNLLCriterion
//import com.intel.analytics.bigdl.dllib.utils.Shape
//import com.intel.analytics.bigdl.dllib.keras.layers._
//import com.intel.analytics.bigdl.numeric.NumericFloat
//import org.apache.spark.ml.feature.VectorAssembler
//import org.apache.spark._
//import org.apache.spark.sql.{SQLContext, SparkSession}
//import org.apache.spark.sql.functions._
//import org.apache.spark.sql.types.DoubleType
//object Example {
//
//
//  def main(args: Array[String]): Unit = {
//
//    val conf = new SparkConf().setMaster("local[2]").setAppName("dllib_demo")
//    val sc = NNContext.initNNContext(conf)
//
////        val spark = new SQLContext(sc) //deprecated
//    val spark = SparkSession
//      .builder()
//      .config(sc.getConf)
//      .getOrCreate()
//
//    val path = "/Users/miconis/Desktop/example_dataset.csv"
//    val df = spark.read.options(Map("inferSchema"->"true","delimiter"->",")).csv(path)
//      .toDF("num_times_pregrant", "plasma_glucose", "blood_pressure", "skin_fold_thickness", "2-hour_insulin", "body_mass_index", "diabetes_pedigree_function", "age", "class")
//
//    val assembler = new VectorAssembler()
//      .setInputCols(Array("num_times_pregrant", "plasma_glucose", "blood_pressure", "skin_fold_thickness", "2-hour_insulin", "body_mass_index", "diabetes_pedigree_function", "age"))
//      .setOutputCol("features")
//    val assembleredDF = assembler.transform(df)
//    val df2 = assembleredDF.withColumn("label", col("class").cast(DoubleType) + lit(1))
//
//    val Array(trainDF, valDF) = df2.randomSplit(Array(0.8, 0.2))
//
//    val x1 = Input(Shape(8))
//    val merge = Merge.merge(inputs = List(x1, x1), mode = "dot")
//    val dense1 = Dense(12, activation="relu").inputs(x1)
//    val dense2 = Dense(8, activation="relu").inputs(dense1)
//    val dense3 = Dense(2, activation="relu").inputs(dense2)
//    val dmodel = Model(x1, dense3)
//
//    dmodel.compile(optimizer = new Adam(), loss = ClassNLLCriterion())
//
//
//    //training
//    dmodel.fit(x = trainDF, batchSize = 4, nbEpoch = 2, featureCols = Array("features"), labelCols = Array("label"), valX = valDF)
//
//
////    //save model
////    val modelPath = "/tmp/demo/keras.model"
////    dmodel.saveModel(modelPath)
////
////
////    //load model
////    val loadModel = Models.loadModel(modelPath)
//    val loadModel = dmodel
//
//    //inference
//    val preDF2 = loadModel.predict(valDF, featureCols = Array("features"), predictionCol = "predict")
//
//    preDF2.show(false)
//
//    //evaluation
//    val ret = dmodel.evaluate(trainDF, batchSize = 4, featureCols = Array("features"), labelCols = Array("label"))
//
//    ret.foreach(println)
//
//  }
//}
