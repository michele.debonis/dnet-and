import eu.dnetlib.featureextraction.Utilities;
import eu.dnetlib.support.Author;
import eu.dnetlib.support.AuthorsFactory;
import eu.dnetlib.support.CoAuthor;
import org.apache.avro.TestAnnotation;
import org.junit.jupiter.api.Test;

import java.lang.annotation.Target;
import java.util.ArrayList;
import java.util.HashMap;

public class UtilityTest {

    @Test
    public void normalizeTest(){
        String cicciopasticcio = Utilities.normalize("cicciÒ pasticcio");

        System.out.println("cicciopasticcio = " + cicciopasticcio);
    }

    @Test
    public void equalTest(){
        System.out.println("true = " + "".equals(""));
    }

    @Test
    public void lnfiTest() throws Exception {
        Author a = new Author("De Bonis, Michele", "Æ", "De Bonis", new ArrayList<CoAuthor>(), "orcid", "author::id", new HashMap<String, double[]>(), "pub::id");
        System.out.println("a = " + a.isAccurate());
        System.out.println(AuthorsFactory.getLNFI(a));
    }

}
